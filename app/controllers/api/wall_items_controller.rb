class Api::WallItemsController < ApplicationController
  #POST api/wall_items?type=message|event {MessageWallItem|EventWallItem}
  #GET api/wall_items

  def index
    list = MyCache.wall_items
    render :json => list
  end

  def create
    if params[:type] == 'message'
      item = MessageWallItem.new
    else
      item = EventWallItem.new
      item.likes = []
      item.invites = []
    end
    item.type = params[:type] == 'event' ? 'event' : 'message'
    item.created_at = Time.now.to_i
    item.author = params[:author]
    unless MyCache.find_user(item.author)
      return validation_failed("user with login '#{item.author}' not found")
    end
    item.to = params[:to]
    if item.to && !MyCache.find_user(item.to)
      return validation_failed("user with login '#{item.to}' not found")
    end
    item.message = params[:message]
    item.id = MyCache.new_wall_item_id

    MyCache.wall_items << item
    LivePublisher.publish('/wall_items', item.as_json)
    render :json => item.to_json
  end

  def like
    item = MyCache.find_wall_item(params[:id])
    user = MyCache.find_user(params[:login])
    item.likes << user
    render :json => item.to_json
  end

  def invite
    item = MyCache.find_wall_item(params[:id])
    user = MyCache.find_user(params[:login])
    item.invites << user
    render :json => item.to_json
  end

  def destroy
    MyCache.wall_items.reject! { |item| item.id == params[:id].to_i }
    render :json => {ok: true}
  end

end

class Api::UsersController < ApplicationController
  def create
    users = MyCache.users
    if params[:login].length < 1
      return validation_failed('login too short')
    elsif user = users.find {|u| u.login == params[:login]}
      return respond_with_user(user)
    elsif !params[:login].match(/^[a-z0-9_\.]+$/)
      return validation_failed("login must match: '/^[a-z0-9_\.]+$/'")
    end
    user = User.new
    user.login = params[:login]
    user.password = params[:password]
    user.created_at = user.last_login = Time.now.to_i
    user.active = true
    users << user
    respond_with_user(user)
  end

  def respond_with_user(user)
    LivePublisher.publish('/users', user.as_json)
    render :json => user.to_json
  end

  def index
    users = MyCache.users
    case params[:type]
      when 'all'
      when 'active'
        users = users.select { |u| u.active }
      when 'inactive'
        users = users.select { |u| !u.active }
    end
    render :json => users
  end

  def show
    users = MyCache.users
    user = users.detect { |u| u.login == params[:id] }
    render :json => user
  end

  def destroy
    users = MyCache.users
    MyCache.users.reject! { |u| u.login == params[:id] }
    render :json => {ok: true}
  end

end

//= require jquery.min
//= require angular.min
//= require angular-resource.min
//= require bootstrap.min
//= require underscore-min
//= require restangular.min
//= require_self


angular.module('rest', ['restangular'])
    .config(function ($routeProvider, $locationProvider, RestangularProvider) {
        RestangularProvider.setBaseUrl("/api");
        RestangularProvider.setRequestInterceptor(function(elem, operation) {
            if (operation === "remove") {
                return undefined;
            }
            return elem;
        });
    });

function UsersCtrl($scope, Restangular) {
    var User = Restangular.all('users');
    $scope.refresh = function() {
        $scope.users = User.getList();
    };
    $scope.refresh();
    $scope.newUser = {login: '', password: ''};

    $scope.post = function() {
        User.post($scope.newUser);
    };
    $scope.delete = function() {

        Restangular.one('users', $scope.newUser.login).remove();
    };

}

function WallItemsCtrl($scope, Restangular) {
    var WallItem = Restangular.all('wall_items');
    $scope.item = {message: '', type: ''};
    $scope.refresh = function() {
        $scope.items = WallItem.getList();
    };
    $scope.refresh();

    $scope.post = function() {
        WallItem.post($scope.item);
    };
    $scope.delete = function() {
        Restangular.one('wall_items', $scope.item.id).remove();
    };
}